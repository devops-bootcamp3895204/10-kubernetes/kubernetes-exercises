FROM amazoncorretto:17.0.9-al2023-headful

ENV DB_SERVER=mysql-primary.test \
    DB_NAME=bXlzcWxkYg== \
    DB_USER=bm9ucm9vdA== \
    DB_PWD=bm9ucm9vdHB3ZA==

# This will install useradd tool
RUN yum -y install shadow-utils

RUN useradd -m -s /bin/sh appuser

USER appuser

RUN mkdir -p /home/appuser/app

COPY build/libs/bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar /home/appuser/app

WORKDIR /home/appuser/app

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "bootcamp-docker-java-mysql-project-1.0-SNAPSHOT.jar"]